/**
	 * 
	 */
	package frameworkdata;

	import org.testng.asserts.SoftAssert;
	import java.io.File;
	import java.io.FileInputStream;
	import java.io.FileNotFoundException;
	import java.io.IOException;
	import java.util.HashMap;
	import java.util.Map;
	import org.apache.logging.log4j.LogManager;
	import org.apache.logging.log4j.Logger;
	import org.apache.poi.ss.usermodel.CellStyle;
	import org.apache.poi.xssf.usermodel.XSSFCell;
	import org.apache.poi.xssf.usermodel.XSSFRow;
	import org.apache.poi.xssf.usermodel.XSSFSheet;
	import org.apache.poi.xssf.usermodel.XSSFWorkbook;
	import org.openqa.selenium.By;
	import org.openqa.selenium.Keys;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.interactions.Actions;
	import org.openqa.selenium.support.ui.ExpectedConditions;
	import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.DataProvider;
	import org.testng.annotations.Test;
	import org.testng.asserts.SoftAssert;

	import com.relevantcodes.extentreports.LogStatus;


	import testbase.base;
	import uiActions.util;




	public class tmv_JobTitle_compare extends base{
		//public static WebDriver driver;
		SoftAssert softAssert = new SoftAssert();
		private static Logger log = LogManager.getLogger(tmv_JobTitle_compare.class.getName());
		util u = new util();
		testbase.Config config = new testbase.Config(prop);
		
		@BeforeTest()
		public void driverinitialize() throws IOException{
			test = rep.startTest("Compare Job Titles");
			test.log(LogStatus.INFO, "Starting the test Compare Job Titles");
			initializeDriver();		
		}

		
		@Test(priority=1)
		public void Login() 
		{
			driver.navigate().to(prop.getProperty("url"));
			test.log(LogStatus.PASS, "Open URL 'https://talentdataexchange.com/brightfield/login/login.html'");
			
			u.LoginToApp(prop.getProperty("username"), prop.getProperty("password"));
			
	        test.log(LogStatus.PASS, "Enter UserName :  kmotarwar" );
			
			test.log(LogStatus.PASS, "Click 'Next' Button" );
			
			test.log(LogStatus.PASS, "Enter PassWord :  9w8Qeru&" );
			
			test.log(LogStatus.PASS, "Click 'Login' Button" );
			
		}
			
			
		    
		    
		
		
		@Test(priority=2)
		public void ApplyMoreFiltersQuery1() throws Exception
		{
			u.click("homepage_truemarketvaluator_id");
		    u.waitForElementVisibility(driver, getElement("truemarketvaluatorpage_jobtitle_xpath"));
		    test.log(LogStatus.PASS, "Click 'Tru Market Valuator' from side menu" );
		    
		    u.click("truemarketvaluatorpage_jobtitle_xpath");
		    test.log(LogStatus.PASS, "Click job titile drop down" );
		    
		    System.out.println(testData("JOB_TITLE_1"));
		    u.type("jobTitleVar1Textbox_xpath", testData("JOB_TITLE_1"));
		    driver.findElement(By.xpath(OR.getProperty("jobTitleVar1ContainsStart_xpath")+testData("JOB_TITLE_1")+OR.getProperty("ContainsEnd_xpath"))).click();
		    test.log(LogStatus.PASS, "Select '"+testData("JOB_TITLE_1")+"' option" );
		    
		    u.click("truemarketvaluatorpage_submit_xpath");
		    test.log(LogStatus.PASS, "Click Submit Button" );
		    
		    String BillRate=u.getElement("payrate_mediumval_xpath").getText();
		    System.out.println(testData("MARKET_BILL_RATE_1")+"="+BillRate);
		    Assert.assertTrue(BillRate.equals(testData("MARKET_BILL_RATE_1")));
		    test.log(LogStatus.PASS, "Market bill rate displayed ["+BillRate+"] matches with expected Market bill rate ["+testData("MARKET_BILL_RATE_1")+"]");
		    takeScreenShot();
		      
		    u.waitForElementClickable(driver, getElement("query2_tmv_xpath"));
			   
		       u.click("query2_tmv_xpath");
		       test.log(LogStatus.PASS, "Click Query2 Button" );
		       
		       u.waitForElementClickable(driver, getElement("tmv_jobtitle2_xpath"));
		       u.click("tmv_jobtitle2_xpath");
			    test.log(LogStatus.PASS, "Click job title drop down" );
		    
		       System.out.println(testData("JOB_TITLE_2"));
			    u.type("jobTitleVar2Textbox_xpath", testData("JOB_TITLE_2"));
			    driver.findElement(By.xpath(OR.getProperty("jobTitleVar2ContainsStart_xpath")+testData("JOB_TITLE_2")+OR.getProperty("ContainsEnd_xpath"))).click();
			    test.log(LogStatus.PASS, "Select '"+testData("JOB_TITLE_2")+"' option" );
			    
			    u.click("tmv_submit2_xpath");
			    test.log(LogStatus.PASS, "Click Submit Button" );
			    
			    String BillRate2=u.getElement("payrate_mediumval2_xpath").getText();
			    System.out.println(testData("MARKET_BILL_RATE_2")+"="+BillRate2);
			    Assert.assertTrue(u.getElement("payrate_mediumval2_xpath").getText().equals(testData("MARKET_BILL_RATE_2")));
			    test.log(LogStatus.PASS, "Market bill rate displayed ["+BillRate2+"] matches with expected Market bill rate ["+testData("MARKET_BILL_RATE_2")+"]");
			    takeScreenShot();

		}
		@AfterTest
		public void closeBrowser(){
			driver.close();
			driver = null; 
			rep.endTest(test);
			rep.flush();
		}
		
}