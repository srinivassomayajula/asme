package frameworkdata;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;

import uiActions.util;

public class contentstaging2 extends testbase.base{
	SoftAssert softAssert = new SoftAssert();
	private static Logger log = LogManager.getLogger(contentstaging2.class.getName());
	testbase.Config config = new testbase.Config(prop);
	util u = new util();

	@BeforeTest
		public void driverinitialize() throws IOException{
			test = rep.startTest("contentstaging2");
			test.log(LogStatus.INFO, "Content stating test case 2");
			log.debug("starting test");
			initializeDriver();
			
		
			
		}

		
	@Test
	public void testname() throws IOException, InterruptedException, AWTException {
		
		
		driver.navigate().to(prop.getProperty("url"));
		test.log(LogStatus.PASS, prop.getProperty("url")+" User lands on contentstaging website.");
		takeScreenShot();
		u.click("Learning_and_development_xpath");
		u.click("Online_self_study_xpath");
		u.click("Format_xpath");
		util.scrollTo(driver, getElement("Print_book_xpath"));
		u.waitToLoad();
		u.click("Print_book_xpath");
		u.click("Apply_button_format_xpath");
		u.waitToLoad();
		util.scrollTo(driver, getElement("Format_xpath"));
		u.waitToLoad();
		u.click("Sciences_and_engineering_xpath");
		u.click("Add_to_cart_button_xpath");
		u.waitToLoad();
		u.click("View_cart_button_xpath");
		u.click("Check_out_button_xpath");
		driver.switchTo().frame("signInIframe");
		getElement("Email_id_xpath").sendKeys(prop.getProperty("username2"));
		getElement("Password_xpath").sendKeys(prop.getProperty("password"));
		u.click("Continue_button_xpath");
		driver.switchTo().defaultContent();
		/*
		util.scrollTo(driver, getElement("You_adress_xpath"));
		u.waitToLoad();
		u.click("You_adress_xpath");
		u.waitToLoad();
		u.click("Register_attendee_xpath");
		u.waitToLoad();
		util.scrollTo(driver, getElement("Order_summary_xpath"));
		u.click("CheckOut_button_xpath");
		*/
		u.waitToLoad();
		u.click("Use_address_button2_xpath");
		u.click("Select_here_xpath");
		u.click("Shipping_options_xpath");
		util.scrollTo(driver, getElement("Save_and_continue_button_shipping_xpath"));
		u.waitToLoad();
		u.click("Save_and_continue_button_shipping_xpath");
		u.click("Billing_address_xpath");
		u.click("Save_and_continue_billing_xpath");
		u.waitToLoad();
		driver.switchTo().frame("CCIframe");
		getElement("Credit_card_number_xpath").sendKeys(prop.getProperty("card_number"));
		getElement("First_name_xpath").sendKeys(prop.getProperty("firstname"));
		getElement("Last_name_xpath").sendKeys(prop.getProperty("lastname"));
		getElement("Validity_Month_Year_xpath").sendKeys(prop.getProperty("expiry_month_year"));
		getElement("CVC_number_xpath").sendKeys(prop.getProperty("cvv"));
		driver.switchTo().defaultContent();
		util.scrollTo(driver, getElement("Place_order_xpath"));
		u.waitToLoad();
		u.click("Place_order_xpath");
		u.waitToLoad();
		String OrderConfirmation = getElement("Order_Confirmation2_xpath").getText();
		test.log(LogStatus.PASS, " Verifiying Order confirmation" + "[" + OrderConfirmation + "]" );
		takeScreenShot();
		String OrderedProduct = getElement("Ordered_product_xpath").getText();
		String Quantity = getElement("Quantity_Text_xpath").getText();
		String QuantityCount = getElement("Quantity_count_xpath").getText();
		String UnitPrice = getElement("Unit_price_text_xpath").getText();
		String UnitPriceAmount = getElement("Unit_price_amount_xpath").getText();
		String TotalPrice = getElement("Total_price_text_xpath").getText();
		String TotalPriceAmount = getElement("Total_price_amount_xpath").getText();
		String ProductFormat = getElement("Product_format_xpath").getText();
		String ProductFormatItem = getElement("Product_format_item_name_xpath").getText();
		util.scrollTo(driver, getElement("Order_total_xpath"));
		String SubTotal = getElement("Subtotal_xpath").getText();
		String SubTotalValue = getElement("Subtotal_value_xpath").getText();
		String Shipping = getElement("Shipping_xpath").getText();
		String ShippingValue = getElement("Shipping_value_xpath").getText();
		String SalesTax = getElement("Sales_tax_xpath").getText();
		String SalesTaxValue = getElement("Sales_tax_value_xpath").getText();
		String OrderTotal = getElement("Order_total_xpath").getText();
		String OrderTotalValue = getElement("Order_total_value_xpath").getText();
		test.log(LogStatus.PASS, " Verifiying Order details" + "[" + OrderedProduct + "] ["+ Quantity +" "+ QuantityCount +"] [" + UnitPrice +" "+ UnitPriceAmount + "] [" + TotalPrice +" "+ TotalPriceAmount + "]" );                               
		test.log(LogStatus.PASS, " Verifiying Order details" + "[" + OrderedProduct + "] ["+ ProductFormat + " " + ProductFormatItem +"]" );                               
		test.log(LogStatus.PASS, " Verifiying Order details" + "[" + OrderedProduct + "] ["+ SubTotal +" "+ SubTotalValue +"] [" + Shipping +" "+ ShippingValue + "] [" + SalesTax +" "+ SalesTaxValue + "] [" + OrderTotal +" "+ OrderTotalValue +"]"); 
		takeScreenShot();
		
		
		
		
		
		
		
		
	}
	
	@AfterTest
	public void closeBrowser(){
	driver.quit();
	driver = null; 
	rep.endTest(test);
	rep.flush();	
	}
	
}