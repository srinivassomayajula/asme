package frameworkdata;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;

import uiActions.util;

public class Facebook extends testbase.base{
	SoftAssert softAssert = new SoftAssert();
	private static Logger log = LogManager.getLogger(Facebook.class.getName());
	testbase.Config config = new testbase.Config(prop);
	util u = new util();

	@BeforeTest
		public void driverinitialize() throws IOException{
			test = rep.startTest("Facebbok");
			test.log(LogStatus.INFO, "//");
			log.debug("starting test");
			initializeDriver();
			
		
			
		}

		
	@Test
	public void testname() throws IOException, InterruptedException, AWTException {
		
		
		
			
		//step 1
		//driver.navigate().to("https://contentstaging12.asme.org");
		driver.navigate().to(prop.getProperty("url"));
		test.log(LogStatus.PASS, prop.getProperty("url")+" User lands on facebook website.");
		
	
		
		u.waitToLoad();
		util.scrollTo(driver, getElement("Events_scroll_xpath"));
		u.waitToLoad();
		//getElement("Emaiid_xpath").sendKeys(testData("username"));
		getElement("Email_id_xpath").sendKeys(prop.getProperty("username"));
		test.log(LogStatus.PASS, "Entering user name");
		
		//getElement("Password_xpath").sendKeys(testData("password"));
		getElement("Email_id_xpath").sendKeys(prop.getProperty("password"));
		test.log(LogStatus.PASS, "Entering password");
		u.click("Login_xpath");
		test.log(LogStatus.PASS, "Logging to facebook");
		takeScreenShot();
		u.waitToLoad();
		takeScreenShot();
	}
	@AfterTest
	public void closeBrowser(){
	driver.quit();
	driver = null; 
	rep.endTest(test);
	rep.flush();	
	}
}